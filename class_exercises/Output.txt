After 39 iterations:

   8.8	  23.2	  48.9	 100.0	
   1.4	   0.0	  15.4	-100.0	
  -2.6	  -1.3	   3.9	  -6.0	

Best policy:

E   E   E   +   
N   #   N   -   
N   E   N   S   
crawfordb@aldenv157:~/cs370f2016/cs370f2016-share/in-class/nov11-ValueIteration$ javac GridWorld.java
crawfordb@aldenv157:~/cs370f2016/cs370f2016-share/in-class/nov11-ValueIteration$ java GridWorld
After 11 iterations:

  -3.3	  -2.7	   5.0	 100.0	
  -3.3	   0.0	  -3.2	-100.0	
  -3.3	  -3.3	  -3.3	  -3.3	

Best policy:

E   E   E   +   
N   #   W   -   
N   E   N   S   
crawfordb@aldenv157:~/cs370f2016/cs370f2016-share/in-class/nov11-ValueIteration$ javac GridWorld.java
crawfordb@aldenv157:~/cs370f2016/cs370f2016-share/in-class/nov11-ValueIteration$ java GridWorld
After 7 iterations:

  91.0	  94.0	  97.0	 100.0	
  88.0	   0.0	  94.0	-100.0	
  85.0	  88.0	  91.0	  88.0	

Best policy:

E   E   E   +   
N   #   N   -   
E   E   N   W   
crawfordb@aldenv157:~/cs370f2016/cs370f2016-share/in-class/nov11-ValueIteration$ javac GridWorld.java
crawfordb@aldenv157:~/cs370f2016/cs370f2016-share/in-class/nov11-ValueIteration$ java GridWorld
After 265 iterations:

  55.0	  62.5	  77.5	 100.0	
  41.2	   0.0	  58.7	-100.0	
  35.0	  37.5	  47.5	  40.0	

Best policy:

N   E   N   +   
E   #   W   -   
E   S   E   S   

